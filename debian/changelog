calls (0.1.6-1mobian2) unstable; urgency=medium

  * d/patches: Include latest upstream commits

 -- Evangelos Ribeiro Tzaras <devrtz-debian@fortysixandtwo.eu>  Sun, 28 Jun 2020 14:58:20 +0200

calls (0.1.6-1mobian1) unstable; urgency=medium

  * debian: add Mobian-specific patches

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 12 Jun 2020 09:44:15 +0200

calls (0.1.6) amber-phone; urgency=medium

  [ Guido Günther ]
  * debian: Add gbp.conf.
    This makes correct taging so much simpler
  * po: Add plugins to POTFILES.skip.  They're not meant to be translated
  * Drop translations from property names and descriptions.
    They're never used anywhere. (Closes: #155, #121)
  * po: Update pot file.
  * calls-contacts: Document lookup_phone_number.
    This makes sure we have clear ownership rules
  * calls-call-record-row: Ref the returned contact
    calls_contacts_lookup_phone_number is transfer-none
  * build: Specifify glib versions.
    This avoids deprecation warnings due to GTimeVal used in
    libebook-contacts.h
  * ringer: Make more functions static
  * ringer: Use libfeedback.
    This triggers e.g. haptic feeback as well and can later on be used
    for LED feedback.
    We can drop the audio theme setting completely since feedbackd picks up
    the global GNOME setting.
  * tests: Disable atk bridge.
    Otherwise this can trigger test failures. Similar to what we
    did in libhandy.
  * Show notification on missed calls (Closes: #153)

  [ Zander Brown ]
  * po: Add British English translation

  [ Antonio Pandolfo ]
  * po: Add Italian translation

  [ Daniel Șerbănescu ]
  * po: Add Romanian translation

  [ Valéry Febvre ]
  * po: Add French translation

  [ Mohammed Sadiq ]
  * main-window: Don’t set program-name in about dialog
    program-name will be retrieved with g_get_application_name().
    Let’s not do it explicitly so that there is one less string
    to translate
  * call-display: Use real ellipsis.
    See https://wiki.gnome.org/Design/OS/Typography

  [ Rafael Fontenelle ]
  * Add Brazilian Portuguese translation

  [ Scott Anecito ]
  * po: Add Japanese translation
  * po: Add ja.po to LINGUAS

  [ Yuri Chornoivan ]
  * Use an ellipsis in translation for Ukrainian

  [ Evangelos Ribeiro Tzaras ]
  * record-store: Use g_autoptr(GError) to avoid having to g_error_free
  * Implement delete call with context menu on longpress
    (Closes: #154)

 -- Guido Günther <agx@sigxcpu.org>  Thu, 11 Jun 2020 09:06:40 +0200

calls (0.1.5) amber-phone; urgency=medium

  [ Guido Günther ]
  * Update POTFILES.in.  See: #146
  * Build against Debian bullseye

  [ Danial Behzadi ]
  * Add Persian
  * po: fa: Update some strings

  [ David Heidelberg ]
  * data: rename appdata to metainfo.  Appstream nowadays prefers
    `.metainfo.xml`

  [ Evangelos Ribeiro Tzaras ]
  * data: Clarify metainfo copyright to be CC0-1.0.
    Adust debian/copyright accordingly.
  * Build L5 specific code in calls-call-display conditionally

  [ Sebastian Rasmussen ]
  * po: Add Swedish translation

  [ Yuri Chornoivan ]
  * po: Add Ukrainian translation

 -- Guido Günther <agx@sigxcpu.org>  Mon, 18 May 2020 11:00:52 +0200

calls (0.1.4) amber-phone; urgency=medium

  [ Julian Sparber ]
  * DummyPlugin: fix events emits for call-removed, origin-*
  * MMPlugin: emit *-removed after removing object
  * Add a CallsManager and move Provider handling to it
  * Manager: add test
  * Use CallsManager in MainWindow and NewCallBox
  * CallWindow: use Manager
  * Ringer: fix parent instance
  * Ringer: Use Manager
  * RecordStore: Use Manager
  * Remove Enumerate and EnumerateParams class
  * HistoryBox: remove new-call property
  * MainWindow: show error message when making calls isn't possible
  * Add HdyAvatar
  * History: use HdyAvatar
  * History: Make avatars 48px big

 -- Julian Sparber <julian.sparber@puri.sm>  Tue, 31 Mar 2020 17:36:42 +0200

calls (0.1.3) amber-phone; urgency=medium

  [ Julian Sparber ]
  * NewCallBox+CallDisplay: UI facelift
  * Use correct parent class when chaining up overridden functions
  * Flatpak: use always a space before :
  * Flatpak: Fix flatpak manifest, remove java dep and update deps
  * Add class for in-app notification
  * MainWindow: use in-app notification for messages
  * CallWindow: use in-app notification
  * CallHistory: Use dial action for recall button
  * CallHistory: Don't pass the CallsNewCallBox to the CallsHistory
  * CallHistory: Add a frame to the call history
  * CallHistory: Disable selecting/activating rows and ellipzise
  * Add .gitignore
  * Set Julian Sparber as maintainer

  [ Bob Ham ]
  * application: Add "--dial" command-line option

  [ Daniel Abrecht ]
  * Fix some lintian warnings regarding the debian/copyright file.
  * Convert calls call getters to readonly properties
  * Notify if number changes in mm plugin
  * Reintroduce GParamSpec props arrays & replace g_object_notify with g_object_notify_by_pspec

  [ tor sorensen ]
  * Add license COPYING file for appropriate packaging.

 -- Julian Sparber <julian.sparber@puri.sm>  Thu, 26 Mar 2020 10:25:00 +0100

calls (0.1.2) amber-phone; urgency=medium

  [ Guido Günther ]
  * debian: Add superficial autopkgtest
  * gitlab-ci: Specify stages
  * gitlab-ci: Build package and run autopkgtest / lintian
  * po: Update pofiles.in (Closes: #76)
  * Add potfile

  [ Mohammed Sadiq ]
  * mm-provider: Fix a check to remove device

  [ Bob Ham ]
  * call-display: Only mute the microphone with the Mute button
  * mm-call: Sanitise disconnect messages

  [ Julian Sparber ]
  * Replace HdyDialer with HdyKeypad and bump libhandy to 0.0.12

 -- Bob Ham <bob.ham@puri.sm>  Mon, 27 Jan 2020 14:55:08 +0000

calls (0.1.1) amber-phone; urgency=medium

  [ Bob Ham ]
  * ringer: Use the GTK sound theme
  * Add phone number lookup using libfolks
  * Use libfolks phone number lookup in call record display
  * Add -Wno-error=deprecated-declarations to build arguments
  * call-display: Add ugly, ugly hacks to enable speakerphone/mute buttons
  * Add initial avatar support

  [ Lubomir Rintel ]
  * appdata.xml: fix a validation error

 -- Bob Ham <bob.ham@puri.sm>  Tue, 10 Dec 2019 14:51:21 +0000

calls (0.1.0) purple; urgency=medium

  [ David Cordero ]
  * sm.puri.Calls.json: Fix libhandy config option type

  [ Mohammed Sadiq ]
  * window: hide windows on delete
  * new-call-box: Use null-terminated string to set as buffer text

  [ Christopher Davis ]
  * history-header-bar: Change "About" to "About Calls"

  [ Tobias Bernard ]
  * Minor: Change recent calls empty state label
  * Switcher: symbolic icons, label capitalization

  [ Bob Ham ]
  * calls-call: Add inbound property
  * plugins/mm: Implement inbound property
  * plugins/dummy: Implement inbound property
  * calls-dummy-provider: Create an inbound call on SIGUSR1
  * Update UI to better reflect the design
  * calls-call: Fix crash with null call state
  * calls-main-window: Use HdyViewSwitcher and friends
  * src: Remove extraneous classes, Calls{History,NewCall}HeaderBar
  * Specify minimum libhandy version in meson and Debian packaging
  * calls-call-display: Add a display of dial pad digits
  * util: Fix off-by-one in calls_entry_append
  * calls-mm-call: Map MM_CALL_STATE_UNKNOWN to CALLS_CALL_STATE_DIALING instead of 0
  * calls-call: Add calls_call_get_inbound function
  * calls-dummy-origin: Fix ordering of state change and call removal callbacks
  * Record calls to an SQLite database via libgom
  * Hook up Recent Calls list to database
  * appdata: Fix application ID
  * Support opening of tel: URIs
  * calls-application: Add --daemon option to not display main window
  * Start up in daemon mode when GNOME starts
  * calls-new-call-box: Allow entering of "+" symbol
  * Use GtkApplication::register-session property to register
  * Display call window over the phosh lockscreen
  * ui/main-window: Hide contacts pane
  * Release calls 0.1.0

  [ Julian Sparber ]
  * flatpak: fix ModemManager
  * docs: update depedencies instructions

  [ David Boddie ]
  * Override the input method for entry widgets

 -- Bob Ham <bob.ham@puri.sm>  Wed, 18 Sep 2019 15:13:00 +0100

calls (0.0.1) purple; urgency=low

  * Initial release.

 -- Bob Ham <rah@settrans.net>  Tue, 04 Dec 2018 11:42:21 +0100
